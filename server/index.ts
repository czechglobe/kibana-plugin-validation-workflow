import { PluginInitializerContext } from '../../../src/core/server';
import { ValidationWorkflowPlugin } from './plugin';

//  This exports static code and TypeScript types,
//  as well as, Kibana Platform `plugin()` initializer.

export function plugin(initializerContext: PluginInitializerContext) {
  return new ValidationWorkflowPlugin(initializerContext);
}

export { ValidationWorkflowPluginSetup, ValidationWorkflowPluginStart } from './types';
