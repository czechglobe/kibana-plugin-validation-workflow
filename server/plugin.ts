import axios from "axios";
import {
  PluginInitializerContext,
  CoreSetup,
  CoreStart,
  Plugin,
  Logger,
} from '../../../src/core/server';
import { schema } from '@kbn/config-schema';

import { ValidationWorkflowPluginSetup, ValidationWorkflowPluginStart } from './types';

export class ValidationWorkflowPlugin
  implements Plugin<ValidationWorkflowPluginSetup, ValidationWorkflowPluginStart> {
  private readonly logger: Logger;

  constructor(initializerContext: PluginInitializerContext) {
    this.logger = initializerContext.logger.get();
  }

  public setup(core: CoreSetup) {
    this.logger.debug('validation-workflow: Setup');
    const router = core.http.createRouter();

    // Register server side APIs
    const proxy_handler = async (context, req, res) => {
      const in_res = await axios.request({
        url: req.params.api_path,
        method: req.route.method,
        baseURL: "http://validation-workflow-api:6542/",
        data: req.body,
        params: req.query,
        responseType: 'stream',
        validateStatus: status => true,
      });
      return res.custom({
        body: in_res.data,
        statusCode: in_res.status,
        headers: in_res.headers,
      });
    };

    const proxy_methods = ["get", "post", "put", "patch", "delete"];
    proxy_methods.forEach(method => {
      router[method](
        {
          path: '/api/validation-workflow/{api_path*}',
          validate: {
            params: schema.any(),
            query: schema.any(),
            body: schema.any(),
          },
          options: {
            body: {
              output: 'data',
              parse: true,
            }
          }
        },
        proxy_handler
      );
    });

    return {};
  }

  public start(core: CoreStart) {
    this.logger.debug('validation-workflow: Started');
    return {};
  }

  public stop() {}
}
