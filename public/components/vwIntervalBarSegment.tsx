import React, {useState} from 'react';
import {EuiPopover} from '@elastic/eui';

export default function VwIntervalBarSegment(props) {
  const { interval } = props;

  const [isPopoverOpen, setIsPopoverOpen] = useState(false);

  const segmentStyle = {
    left: interval.start + '%',
    width: interval.end - interval.start + '%',
    backgroundColor: interval.color,
  };

  return (
    <div className="vwIntervalBar-interval" style={segmentStyle}>
      <EuiPopover
        display="block"
        anchorPosition="upCenter"
        button={
          <button
            className="vwIntervalBar-interval-button"
            onClick={() => setIsPopoverOpen(isPopoverOpen => !isPopoverOpen)}
          />
        }
        isOpen={isPopoverOpen}
        closePopover={() => setIsPopoverOpen(false)}
      >
        {interval.content}
      </EuiPopover>
    </div>
  );
}
