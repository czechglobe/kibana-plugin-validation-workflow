import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {EuiComboBox, EuiFlexGroup, EuiFlexItem, EuiFormRow, EuiSelect} from '@elastic/eui';
import creators from '../services/creators';

class VwMetricsSelector extends React.Component {
  render() {
    const { step, location, locations, metrics, selectedMetrics } = this.props;

    const stepOptions = [
      { value: undefined, text: '' },
      { value: 'raw', text: 'Raw' },
      { value: 'unit', text: 'Unit' },
      { value: 'final', text: 'Final' },
    ];

    const locationOptions = locations.map(l => {
      return {
        value: l.id,
        text: l.title,
      };
    });
    locationOptions.unshift({ value: undefined, text: '' });

    const primaryMetrics = [];
    if (location) {
      metrics.forEach(loc => {
        loc.options.forEach(m => {
          if (m._location === location) {
            primaryMetrics.push(m);
          }
        });
      });
    }

    const toMetricOption = m => {
      return {
        _location: m.location,
        _metric: m.metric,
        label: m.metric,
      };
    };
    const selectedMetricsPrimary = selectedMetrics.filter(m => m.primary).map(toMetricOption);
    const selectedMetricsSecondary = selectedMetrics.filter(m => !m.primary).map(toMetricOption);

    return (
      <EuiFlexGroup>
        <EuiFlexItem grow={1}>
          <EuiFormRow label="Select step">
            <EuiSelect
              placeholder="Select step"
              options={stepOptions}
              value={step}
              onChange={e => this.props.onChangeStep(e.target.value)}
              aria-label="Select step of data pipeline to validate"
            />
          </EuiFormRow>
        </EuiFlexItem>
        <EuiFlexItem grow={1}>
          <EuiFormRow label="Select location">
            <EuiSelect
              placeholder="Select location"
              options={locationOptions}
              value={location}
              onChange={e => this.props.onChangeLocation(e.target.value)}
              aria-label="Select location to validate"
            />
          </EuiFormRow>
        </EuiFlexItem>
        <EuiFlexItem grow={2}>
          <EuiFormRow label="Metrics to validate (primary)" fullWidth>
            <EuiComboBox
              placeholder="Metrics to validate (primary)"
              options={primaryMetrics}
              selectedOptions={selectedMetricsPrimary}
              onChange={this.props.onChangeMetricsPrimary}
              fullWidth
            />
          </EuiFormRow>
        </EuiFlexItem>
        <EuiFlexItem grow={2}>
          <EuiFormRow label="Metrics to show (secondary)" fullWidth>
            <EuiComboBox
              placeholder="Metrics to show (secondary)"
              options={metrics}
              selectedOptions={selectedMetricsSecondary}
              onChange={this.props.onChangeMetricsSecondary}
              fullWidth
            />
          </EuiFormRow>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
  }
}

export default connect(
  state => {
    return {
      step: state.selector.step,
      location: state.selector.location,
      locations: state.globals.locations,
      metrics: state.metrics,
      selectedMetrics: state.selector.metrics,
    };
  },
  dispatch => {
    return bindActionCreators(
      {
        onChangeStep: creators.onChangeStep,
        onChangeLocation: creators.onChangeLocation,
        onChangeMetricsPrimary: creators.onChangeMetricsPrimary,
        onChangeMetricsSecondary: creators.onChangeMetricsSecondary,
      },
      dispatch
    );
  }
)(VwMetricsSelector);
