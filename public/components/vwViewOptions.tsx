import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {EuiButtonEmpty, EuiFlexGroup, EuiFlexItem, EuiSwitch} from '@elastic/eui';
import creators from '../services/creators';

function VwViewOptions(props) {
  const {
    showOriginal,
    showQcode,
    onChangeShowOriginal,
    onChangeShowQcode,
    openDataInspector,
  } = props;

  return (
    <EuiFlexGroup>
      <EuiFlexItem grow={false}>
        <EuiSwitch
          label="Show original"
          checked={showOriginal}
          onChange={e => onChangeShowOriginal(e.target.checked)}
          compressed
        />
      </EuiFlexItem>
      <EuiFlexItem grow={false}>
        <EuiSwitch label="Show qcode" checked={showQcode} onChange={e => onChangeShowQcode(e.target.checked)} compressed />
      </EuiFlexItem>
      <EuiFlexItem grow={false}>
        <EuiButtonEmpty size="xs" onClick={openDataInspector}>
          Show data
        </EuiButtonEmpty>
      </EuiFlexItem>
      <EuiFlexItem />
    </EuiFlexGroup>
  );
}

export default connect(
  state => {
    return {
      showOriginal: state.selector.showOriginal,
      showQcode: state.selector.showQcode,
    };
  },
  dispatch => {
    return bindActionCreators(
      {
        onChangeShowQcode: creators.onChangeShowQcode,
        onChangeShowOriginal: creators.onChangeShowOriginal,
        openDataInspector: creators.openDataInspector,
      },
      dispatch
    );
  }
)(VwViewOptions);
