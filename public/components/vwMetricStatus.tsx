import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import moment from 'moment';
import {stringify as toQueryString} from 'querystring';
import {EuiButton, EuiFlexGroup, EuiFlexItem, EuiPopoverFooter, EuiPopoverTitle, EuiTextColor,} from '@elastic/eui';
import VwIntervalBar from './vwIntervalBar';
import VwRange from './vwRange';
import creators from '../services/creators';

const stateMap = {
  undefined: {
    tooltip: '',
    color: '#d3dae6',
  },
  validated: {
    tooltip: 'Validated ✔️',
    color: '#017d73',
  },
  nonvalidated: {
    tooltip: 'Not validated',
    color: '#bd271e',
  },
  success: {
    tooltip: 'Success',
    color: '#017d73',
  },
  failed: {
    tooltip: 'Failed',
    color: '#bd271e',
  },
  retry: {
    tooltip: 'Retry',
    color: '#fdd009',
  },
  waiting: {
    tooltip: 'Waiting',
    color: '#006bb4',
  },
};

class VwMetricStatus extends React.Component {
  openAirflowLogs(start, end) {
    const data = {
      root: '',
      dag_id: this.props.location,
      base_date: end.format('YYYY-MM-DD HH:mm:ss'),
      num_runs: Math.ceil(moment.duration(end - start).asDays()),
    };
    const url = this.props.airflowBaseUrl + '/admin/airflow/tree?' + toQueryString(data);
    window.open(url, '_blank');
  }

  processIntervals(intervals) {
    const parsedIntervals = intervals.map(i => {
      return {
        start: moment(i.start),
        end: moment(i.end),
        status: i.status,
      };
    });

    const min = parsedIntervals.map(i => i.start).reduce((a, b) => (a < b ? a : b));
    const max = parsedIntervals.map(i => i.end).reduce((a, b) => (a > b ? a : b));
    const size = max.diff(min);

    return parsedIntervals.map(i => {
      const stateConfig = stateMap[i.status];
      return {
        start: 100 * (i.start.diff(min) / size),
        end: 100 * (i.end.diff(min) / size),
        tooltip: stateConfig.tooltip,
        color: stateConfig.color,
        content: (
          <div>
            <EuiPopoverTitle>{stateConfig.tooltip}</EuiPopoverTitle>
            <EuiFlexGroup>
              <EuiFlexItem>
                <EuiButton
                  onClick={() =>
                    this.props.onTimeChange(i.start.toISOString(), i.end.toISOString())
                  }
                  iconType="magnifyWithPlus"
                  size="s"
                  fullWidth
                >
                  Zoom
                </EuiButton>
              </EuiFlexItem>
              <EuiFlexItem>
                <EuiButton
                  onClick={() => this.openAirflowLogs(i.start, i.end)}
                  iconType="menuRight"
                  size="s"
                  fullWidth
                >
                  Show airflow logs
                </EuiButton>
              </EuiFlexItem>
            </EuiFlexGroup>
            <EuiPopoverFooter>
              <EuiTextColor color="subdued">
                <VwRange
                  start={i.start.format('YYYY-MM-DD HH:mm:ss')}
                  end={i.end.format('YYYY-MM-DD HH:mm:ss')}
                />
              </EuiTextColor>
            </EuiPopoverFooter>
          </div>
        ),
      };
    });
  }

  render() {
    const { location, metricStatus } = this.props;

    let rawIntervals;
    let unitIntervals;
    let finalIntervals;
    if (location && metricStatus) {
      rawIntervals = this.processIntervals(metricStatus.raw);
      unitIntervals = this.processIntervals(metricStatus.unit);
      finalIntervals = this.processIntervals(metricStatus.final);
    } else {
      rawIntervals = unitIntervals = finalIntervals = [
        {
          start: 0,
          end: 100,
          from_label: '?',
          to_label: '?',
          tooltip: stateMap.undefined.tooltip,
          color: stateMap.undefined.color,
        },
      ];
    }

    return (
      <EuiFlexGroup gutterSize="m" direction="column">
        <EuiFlexItem>
          <VwIntervalBar intervals={rawIntervals} />
        </EuiFlexItem>
        <EuiFlexItem>
          <VwIntervalBar intervals={unitIntervals} />
        </EuiFlexItem>
        <EuiFlexItem>
          <VwIntervalBar intervals={finalIntervals} />
        </EuiFlexItem>
      </EuiFlexGroup>
    );
  }
}

export default connect(
  state => {
    return {
      location: state.selector.location,
      metricStatus: state.metricStatus,
      airflowBaseUrl: state.globals.airflowBaseUrl,
    };
  },
  dispatch => {
    return bindActionCreators(
      {
        onTimeChange: creators.onTimeChange,
      },
      dispatch
    );
  }
)(VwMetricStatus);
