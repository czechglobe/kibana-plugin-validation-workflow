import React from 'react';
import {connect} from 'react-redux';
import {isEqual} from 'lodash';
import {bindActionCreators} from 'redux';
import {VwVisHandler} from './vwVisHandler';
import creators from '../services/creators';

class VwVis extends React.Component {
  constructor(props) {
    super(props);
    this.visRef = React.createRef();
    this.visHandler = new VwVisHandler(props.embeddable, props.savedObjects);
    this.props.initVwVisHandler(this.visHandler);
    this.initializing = undefined;
    this.refresh = false;
  }

  refreshVisWhenReady() {
    if (!this.initializing) {
      this.visHandler.destroy();

      this.initializing = this.visHandler.render(
        this.visRef.current,
        this.props.metrics,
        this.props.onBrushChange,
        this.props.timeRange,
        this.props.locationFrequency,
        this.props.showOriginal
      );
      this.initializing
        .then(() => {
          this.initializing = undefined;
          if (this.refresh) {
            this.refresh = false;
            this.refreshVisWhenReady();
          }
        })
        .catch(err => {
          this.props.showApiError(err, 'Error while rendering vis');
          this.initializing = undefined;
          if (this.refresh) {
            this.refresh = false;
            this.refreshVisWhenReady();
          }
        });
    } else {
      this.refresh = true;
    }
  }

  shouldComponentUpdate() {
    return true;
  }

  componentDidMount() {
    this.refreshVisWhenReady();
  }

  componentDidUpdate(prevProps) {
    if (
      !isEqual(this.props.metrics, prevProps.metrics) ||
      this.props.showOriginal !== prevProps.showOriginal
    ) {
      this.refreshVisWhenReady();
    } else {
      if (!isEqual(this.props.timeRange, prevProps.timeRange)) {
        this.visHandler.update(this.props.timeRange);
      } else {
        this.visHandler.reload();
      }
    }
  }

  componentWillUnmount() {
    this.visHandler.destroy();
  }

  render() {
    return (
      <div
        className="validation-workflow-vis"
        style={{ height: '80vh', display: 'flex' }}
        ref={this.visRef}
      />
    );
  }
}

export default connect(
  state => {
    return {
      metrics: state.selector.metrics,
      timeRange: state.selector.timeRange,
      locationFrequency: state.selector.locationFrequency,
      showOriginal: state.selector.showOriginal,
      embeddable: state.services.embeddable,
      savedObjects: state.services.savedObjects,
    };
  },
  dispatch => {
    return bindActionCreators(
      {
        showApiError: creators.showApiError,
        onBrushChange: creators.onBrushChange,
        initVwVisHandler: creators.initVwVisHandler,
      },
      dispatch
    );
  }
)(VwVis);
