import React from 'react';
import VwIntervalBarSegment from './vwIntervalBarSegment';

export default function VwIntervalBar(props) {
  const { intervals } = props;

  return (
    <div className="vwIntervalBar">
      {intervals.map((interval, index) => {
        return <VwIntervalBarSegment key={index} interval={interval} />;
      })}
    </div>
  );
}
