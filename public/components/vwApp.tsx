import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    EuiFlexGroup,
    EuiFlexItem,
    EuiFormRow,
    EuiPage,
    EuiPageBody,
    EuiPageContent,
    EuiPageHeader,
    EuiSuperDatePicker,
    EuiText,
} from '@elastic/eui';
import creators from '../services/creators';

import VwVis from './vwVis';
import VwValidationPanel from './vwValidationPanel';
import VwMetricsSelector from './vwMetricsSelector';
import VwZoomControls from './vwZoomControls';
import VwViewOptions from './vwViewOptions';
import VwMetricStatus from './vwMetricStatus';

class VwApp extends React.Component {
  render() {
    const { timeRange, brushRange, metrics } = this.props;

    return (
      <EuiPage style={{ height: '100%' }}>
        <EuiPageBody>
          <EuiPageHeader>
            <EuiFlexGroup direction="column" gutterSize="s">
              <EuiFlexItem>
                <EuiFlexGroup>
                  <EuiFlexItem grow={true}>
                    <VwMetricsSelector />
                  </EuiFlexItem>
                  <EuiFlexItem grow={false}>
                    <EuiFormRow label="Zoom" display="center">
                      <VwZoomControls />
                    </EuiFormRow>
                  </EuiFlexItem>
                  <EuiFlexItem grow={false}>
                    <EuiFormRow label="Date range" fullWidth>
                      <EuiSuperDatePicker
                        start={timeRange.start}
                        end={timeRange.end}
                        onTimeChange={({ start, end }) => this.props.onTimeChange(start, end)}
                        onRefresh={({ start, end }) => this.props.onTimeChange(start, end)}
                      />
                    </EuiFormRow>
                  </EuiFlexItem>
                </EuiFlexGroup>
              </EuiFlexItem>
              <EuiFlexItem>
                <VwViewOptions />
              </EuiFlexItem>
            </EuiFlexGroup>
          </EuiPageHeader>
          <EuiFlexGroup alignItems="stretch">
            <EuiFlexItem grow={7}>
              <EuiFlexGroup direction="column" gutterSize="s">
                <EuiFlexItem grow={1} style={{ minHeight: '80vh' }}>
                  {metrics.length > 0 ? (
                    <VwVis />
                  ) : (
                    <EuiFlexGroup alignItems="center" justifyContent="center">
                      <EuiFlexItem grow={false}>
                        <EuiText>Choose primary and/or secondary metrics to start.</EuiText>
                      </EuiFlexItem>
                    </EuiFlexGroup>
                  )}
                </EuiFlexItem>
                <EuiFlexItem grow={false}>
                  <VwMetricStatus />
                </EuiFlexItem>
              </EuiFlexGroup>
            </EuiFlexItem>
            {brushRange.start && brushRange.end ? (
              <EuiFlexItem grow={3}>
                <EuiPageContent>
                  <VwValidationPanel />
                </EuiPageContent>
              </EuiFlexItem>
            ) : null}
          </EuiFlexGroup>
        </EuiPageBody>
      </EuiPage>
    );
  }
}

export default connect(
  state => {
    return {
      timeRange: state.selector.timeRange,
      brushRange: state.selector.brushRange,
      metrics: state.selector.metrics,
    };
  },
  dispatch => {
    return bindActionCreators(
      {
        onTimeChange: creators.onTimeChange,
      },
      dispatch
    );
  }
)(VwApp);
