import React from 'react';
import {EuiFlexGroup, EuiFlexItem} from '@elastic/eui';

export default function VwRange(props) {
  const { start, end } = props;

  return (
    <EuiFlexGroup justifyContent="spaceBetween">
      <EuiFlexItem grow={false}>{start}</EuiFlexItem>
      <EuiFlexItem grow={false}>—</EuiFlexItem>
      <EuiFlexItem grow={false}>{end}</EuiFlexItem>
    </EuiFlexGroup>
  );
}
