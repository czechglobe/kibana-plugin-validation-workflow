import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import moment from 'moment';
import {EuiButtonIcon, EuiFlexGroup, EuiFlexItem} from '@elastic/eui';
import creators from '../services/creators';

class VwZoomControls extends React.Component {
  zoomOut() {
    const { timeRange, onTimeChange } = this.props;
    const f = moment(timeRange.start);
    const t = moment(timeRange.end);
    const d2 = moment.duration(moment.duration(t - f) / 2);
    onTimeChange(f.subtract(d2).toISOString(), t.add(d2).toISOString());
  }

  zoomIn() {
    const { timeRange, brushRange, onTimeChange } = this.props;
    if (brushRange.start && brushRange.end) {
      onTimeChange(brushRange.start, brushRange.end);
    } else {
      const f = moment(timeRange.start);
      const t = moment(timeRange.end);
      const d2 = moment.duration(t - f) / 4;
      onTimeChange(f.add(d2).toISOString(), t.subtract(d2).toISOString());
    }
  }

  render() {
    return (
      <EuiFlexGroup gutterSize="s" alignItems="center">
        <EuiFlexItem grow={false}>
          <EuiButtonIcon
            onClick={this.zoomOut.bind(this)}
            iconType="magnifyWithMinus"
            aria-label="Zoom out"
          />
        </EuiFlexItem>
        <EuiFlexItem grow={false}>
          <EuiButtonIcon
            onClick={this.zoomIn.bind(this)}
            iconType="magnifyWithPlus"
            aria-label="Zoom in"
          />
        </EuiFlexItem>
      </EuiFlexGroup>
    );
  }
}

export default connect(
  state => {
    return {
      timeRange: state.selector.timeRange,
      brushRange: state.selector.brushRange,
    };
  },
  dispatch => {
    return bindActionCreators(
      {
        onTimeChange: creators.onTimeChange,
      },
      dispatch
    );
  }
)(VwZoomControls);
