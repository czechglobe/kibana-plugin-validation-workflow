import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {capitalize} from 'lodash';
import {EuiButton, EuiFlexGroup, EuiFlexItem, EuiHorizontalRule, EuiIcon, EuiTitle, EuiTreeView,} from '@elastic/eui';
import VwTransformSettings from './vwTransformSettings';
import creators from '../services/creators';
import {getBrushLength} from '../services/utils';

function VwValidationPanel(props) {
  const {
    transforms,
    metrics,
    brushRange,
    editorTransforms,
    editorTransformsChanged,
    editorTransformsSaving,
    editorValidationSaving,
  } = props;
  const { setTransform, onChangeTransformParam, saveTransforms, saveValidated } = props;
  const brushLength = getBrushLength(brushRange);

  let transformsCategories = transforms.map(transform =>
    transform.category ? transform.category : 'Other'
  );
  transformsCategories = transformsCategories.filter((v, i, a) => a.indexOf(v) === i);
  let transformsTree = {};
  transformsCategories.forEach(category => {
    transformsTree[category] = {
      label: category,
      id: category,
      icon: <EuiIcon type="folderClosed" />,
      iconWhenExpanded: <EuiIcon type="folderOpen" />,
      children: [],
    };
  });
  transforms.forEach(transform => {
    transformsTree[transform.category].children.push({
      label: transform.name,
      id: transform.id,
      icon: <EuiIcon type="number" />,
      callback: () => setTransform(brushRange, transform),
    });
  });
  transformsTree = Object.values(transformsTree);

  return (
    <EuiFlexGroup columns={2} direction="column">
      <EuiFlexItem>
        <EuiFlexGroup justifyContent="spaceBetween" alignItems="center">
          <EuiFlexItem>
            <EuiTitle>
              <h2>{brushLength ? capitalize(brushLength.humanize()) : null}</h2>
            </EuiTitle>
          </EuiFlexItem>
          <EuiFlexItem grow={false}>
            <EuiButton
              fill
              onClick={() => saveValidated(metrics, editorTransforms)}
              isDisabled={brushLength == null}
              isLoading={editorValidationSaving}
            >
              Validate
            </EuiButton>
          </EuiFlexItem>
          <EuiFlexItem grow={false}>
            <EuiButton
              fill
              onClick={() => saveTransforms(metrics, editorTransforms)}
              isDisabled={!editorTransformsChanged}
              isLoading={editorTransformsSaving}
            >
              Save
            </EuiButton>
          </EuiFlexItem>
        </EuiFlexGroup>
      </EuiFlexItem>
      <EuiFlexItem>
        <EuiTitle size="s">
          <h3>Current settings</h3>
        </EuiTitle>

        <EuiFlexGroup direction="column">
          {editorTransforms.map((editorTransform, index) => {
            const transform = transforms.find(t => t.id === editorTransform.transformation);
            return (
              <EuiFlexItem key={index}>
                <VwTransformSettings
                  transform={transform}
                  editorTransform={editorTransform}
                  onChangeTransformParam={onChangeTransformParam.bind(this, index)}
                />
              </EuiFlexItem>
            );
          })}
        </EuiFlexGroup>

        <EuiHorizontalRule />

        <EuiTitle size="s">
          <h3>New transformation</h3>
        </EuiTitle>

        <EuiTreeView items={transformsTree} aria-label="Select new transformation" />
      </EuiFlexItem>
    </EuiFlexGroup>
  );
}

export default connect(
  state => {
    return {
      transforms: state.globals.transforms,
      metrics: state.selector.metrics,
      brushRange: state.selector.brushRange,
      editorTransforms: state.editor.transforms,
      editorTransformsChanged: state.editor.transformsChanged,
      editorTransformsSaving: state.editor.transformsSaving,
      editorValidationSaving: state.editor.validateSaving,
    };
  },
  dispatch => {
    return bindActionCreators(
      {
        setTransform: creators.setTransform,
        onChangeTransformParam: creators.onChangeTransformParam,
        saveTransforms: creators.saveTransforms,
        saveValidated: creators.saveValidated,
      },
      dispatch
    );
  }
)(VwValidationPanel);
