import moment from 'moment';

const getVisState = (frequency, metrics) => {
  return {
    "title": "Validation Workflow",
    "type": "line",
    "params": {
      "type": "line",
      "grid": {
        "categoryLines": true
      },
      "categoryAxes": [
        {
          "id": "CategoryAxis-1",
          "type": "category",
          "position": "bottom",
          "show": true,
          "style": {},
          "scale": {
            "type": "linear"
          },
          "labels": {
            "show": true,
            "filter": true,
            "truncate": 100
          },
          "title": {
            "show": false
          }
        }
      ],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "name": "LeftAxis-1",
          "type": "value",
          "position": "left",
          "show": true,
          "style": {},
          "scale": {
            "type": "linear",
            "mode": "normal"
          },
          "labels": {
            "show": true,
            "rotate": 0,
            "filter": false,
            "truncate": 6
          },
          "title": {
            "text": "Primary"
          }
        },
        {
          "id": "ValueAxis-2",
          "name": "RightAxis-1",
          "type": "value",
          "position": "right",
          "show": true,
          "style": {},
          "scale": {
            "type": "linear",
            "mode": "normal"
          },
          "labels": {
            "show": true,
            "rotate": 0,
            "filter": false,
            "truncate": 6
          },
          "title": {
            "text": "Secondary"
          }
        }
      ],
      "seriesParams": metrics.map(m => {
        return {
          "show": true,
          "type": "line",
          "mode": "normal",
          "data": {
            "label": m.metric,
            "id": m.metric
          },
          "valueAxis": m.primary ? "ValueAxis-1" : "ValueAxis-2",
          "drawLinesBetweenPoints": true,
          "lineWidth": m.primary ? 2.5 : 1,
          "interpolate": "linear",
          "showCircles": false,
        };
      }),
      "addTooltip": true,
      "addLegend": true,
      "legendPosition": "bottom",
      "times": [],
      "addTimeMarker": false,
      "labels": {},
      "thresholdLine": {
        "show": false,
        "value": 10,
        "width": 1,
        "style": "full",
        "color": "#E7664C"
      }
    },
    "aggs": [
      {
        "id": "1",
        "enabled": true,
        "type": "date_histogram",
        "schema": "segment",
        "params": {
          "field": "@timestamp",
          "useNormalizedEsInterval": true,
          "scaleMetricValues": false,
          "interval": frequency,
          "drop_partials": true,
          "min_doc_count": 1,
          "extended_bounds": {}
        }
      },
    ].concat(metrics.map(m => {
      return {
        "id": m.metric,
        "enabled": true,
        "type": "avg",
        "schema": "metric",
        "params": {
          "field": m.metric
        }
      }
    }))
  };
};

/**
 * A handler to the embedded visualization. It offers several methods to interact
 * with the visualization.
 */
export class VwVisHandler {

  constructor(embeddable, savedObjects) {
    this.embeddable = embeddable;
    this.savedObjects = savedObjects;
    this.visualizationId = "2c40754a-1304-8186-4c72-353aa394f386";
  }

  public async render(visualizationEl, metrics, onBrushChange, timeRange, locationFrequency, showOriginalIndex) {
    const visualizationFactory = this.embeddable.getEmbeddableFactory('visualization');

    const indexPattern = metrics[0].step + (showOriginalIndex ? "" : "_fixed");
    const attributes = {
      "title": "Validation workflow",
      "description": "",
      "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\"query\":{\"query\":\"\",\"language\":\"kuery\"},\"filter\":[],\"indexRefName\":\"kibanaSavedObjectMeta.searchSourceJSON.index\"}"
      },
      "uiStateJSON": "{}",
      "visState": JSON.stringify(getVisState(locationFrequency, metrics))
    };
    const references = [
      {
        "name": "kibanaSavedObjectMeta.searchSourceJSON.index",
        "type": "index-pattern",
        "id": indexPattern,
      }
    ];
    await this.savedObjects.create("visualization", attributes, {
      "id": this.visualizationId,
      "overwrite": true,
      "references": references
    });

    const params = {
      timeRange: {
        from: moment(timeRange.start).toISOString(),
        to: moment(timeRange.end).toISOString(),
      }
    };

    this.handler = await visualizationFactory.createFromSavedObject(this.visualizationId, params);
    window.handler = this.handler;
    this.handler.render(visualizationEl);

    const visHandler = this.handler.handler;
    if (visHandler) {
      await visHandler.events$.subscribe(async (event) => {
        if (event.name == "brush") {
          if (event.data.range[0].getTime() != event.data.range[1].getTime()) {
            onBrushChange(moment(event.data.range[0]).toISOString(), moment(event.data.range[1]).toISOString());
          } else {
            onBrushChange(undefined, undefined);
          }
        }
      });
    }
  }

  public update(timeRange) {
    if (this.handler) {
      this.handler.updateInput({
        timeRange: {
          from: moment(timeRange.start).toISOString(),
          to: moment(timeRange.end).toISOString(),
        },
      });
    }
  }

  public reload() {
    if (this.handler) {
      this.handler.reload();
    }
  }

  public openDataInspector() {
    if (this.handler) {
      this.handler.openInspector();
    }
  }

  public destroy() {
    if (this.handler) {
      this.handler.destroy();
      this.handler = undefined;
    }
  }
}
