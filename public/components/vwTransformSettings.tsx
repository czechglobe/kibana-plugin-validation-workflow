import React from 'react';
import moment from 'moment';
import {EuiFieldNumber, EuiFieldText, EuiForm, EuiFormRow, EuiPanel, EuiSwitch, EuiText, EuiTitle,} from '@elastic/eui';
import VwRange from './vwRange';

export default function VwTransformSettings(props) {
  const { transform, editorTransform } = props;

  if (!transform || !editorTransform) return null;

  const transformationParamsInputs = transform.params.map(param => {
    const value = editorTransform.transformationParams[param.name] || '';

    const sanitizeInt = value => {
      const s = parseInt(value, 10);
      return isNaN(s) ? '' : s;
    };

    const sanitizeFloat = value => {
      const s = parseFloat(value);
      return isNaN(s) ? '' : s;
    };

    let input;
    if (param.type === 'int') {
      input = (
        <EuiFieldNumber
          name={param.name}
          value={sanitizeInt(value)}
          step={1}
          onChange={e => props.onChangeTransformParam(param.name, sanitizeInt(e.target.value))}
          fullWidth
        />
      );
    } else if (param.type === 'float') {
      input = (
        <EuiFieldNumber
          name={param.name}
          value={sanitizeFloat(value)}
          step={0.001}
          onChange={e => props.onChangeTransformParam(param.name, sanitizeFloat(e.target.value))}
          fullWidth
        />
      );
    } else if (param.type === 'bool') {
      input = (
        <EuiSwitch
          name={param.name}
          label={param.name}
          checked={Boolean(value)}
          onChange={e => props.onChangeTransformParam(param.name, e.target.checked ? true : false)}
        />
      );
    } else {
      input = (
        <EuiFieldText
          name={param.name}
          value={value}
          onChange={e => props.onChangeTransformParam(param.name, e.target.value)}
          fullWidth
        />
      );
    }

    return (
      <EuiFormRow label={param.name} key={param.name} fullWidth>
        {input}
      </EuiFormRow>
    );
  });

  return (
    <EuiPanel paddingSize="m">
      <EuiTitle>
        <h2>{transform.name}</h2>
      </EuiTitle>
      <EuiText size="s">
        <VwRange
          start={moment(editorTransform.start).format('YYYY-MM-DD HH:mm:ss')}
          end={moment(editorTransform.end).format('YYYY-MM-DD HH:mm:ss')}
        />
      </EuiText>
      <EuiForm>{transformationParamsInputs}</EuiForm>
    </EuiPanel>
  );
}
