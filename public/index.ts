import './index.scss';

import {ValidationWorkflowPlugin} from './plugin';

// This exports static code and TypeScript types,
// as well as, Kibana Platform `plugin()` initializer.
export function plugin() {
  return new ValidationWorkflowPlugin();
}
export { ValidationWorkflowPluginSetup, ValidationWorkflowPluginStart } from './types';
