import moment from 'moment';
import {cloneDeep} from 'lodash';
import DateMath from '@elastic/datemath';

import {types} from './types';
import {getBrushLength} from './utils';

const interactiveModeTreashold = moment.duration({ days: 40 });

const showApiError = (err, message) => {
  return (dispatch, getState) => {
    const state = getState();
    const toastNotifications = state.services.toastNotifications;
    if (err) {
      err.message = JSON.stringify(err?.data || err?.message);
      toastNotifications.addError(err, {
        title: 'API error',
        toastMessage: message,
      });
    }
  }
};

const loadTransforms = (metric, brushRange) => {
  return (dispatch, getState) => {
    const state = getState();
    const httpClient = state.services.httpClient;

    const brushLength = getBrushLength(brushRange);
    if (brushLength != null) {
      const query = {
        location: metric.location,
        step: metric.step,
        metric: metric.metric,
        start: DateMath.parse(brushRange.start, {roundUp: false}).toISOString(),
        end: DateMath.parse(brushRange.end, {roundUp: true}).toISOString(),
      };
      httpClient
        .get('../api/validation-workflow/v1/workflow/transformation', {query})
        .then(resp => {
          dispatch({
            type: types.INIT_EDITOR_TRANSFORMS,
            transforms: resp.map(editorTransform => {
              return {
                start: editorTransform.start,
                end: editorTransform.end,
                transformation: editorTransform.transformation,
                transformationParams: editorTransform.transformation_params,
              };
            }),
          });
        })
        .catch(err => {
          dispatch(showApiError(err, '/workflow/transformation'));
        });
    } else {
      dispatch({
        type: types.INIT_EDITOR_TRANSFORMS,
        transforms: [],
      });
    }
  };
};

const loadMetricState = (location, timeRange) => {
  return (dispatch, getState) => {
    const state = getState();
    const httpClient = state.services.httpClient;

    if (location && timeRange.start && timeRange.end) {
      const query = {
        start: DateMath.parse(timeRange.start, {roundUp: false}).toISOString(),
        end: DateMath.parse(timeRange.end, {roundUp: true}).toISOString(),
        location: location,
        type: 'processing',
      };
      httpClient
        .get('../api/validation-workflow/v1/workflow/status/processing', {query})
        .then(res => {
          dispatch({
            type: types.INIT_METRIC_STATUS,
            metricStatus: res,
          });
        })
        .catch(err => {
          dispatch(showApiError(err, '/workflow/status/processing'));
        });
    }
  };
};

const onTimeChange = (start, end) => {
  return (dispatch, getState) => {
    const state = getState();
    dispatch({
      type: types.SET_TIME_RANGE,
      start,
      end,
    });
    dispatch(loadMetricState(state.selector.location, { start, end }));
  };
};

const onBrushChange = (start, end) => {
  return (dispatch, getState) => {
    dispatch({
      type: types.SET_BRUSH_RANGE,
      start,
      end,
    });

    const state = getState();
    const primaryMetrics = state.selector.metrics.filter(m => m.primary);
    const metric = primaryMetrics.length ? primaryMetrics[0] : null;

    dispatch(loadTransforms(metric, { start, end }));
  };
};

const onChangeStep = step => {
  return (dispatch, getState) => {
    const state = getState();
    const httpClient = state.services.httpClient;

    dispatch({
      type: types.SET_STEP,
      step,
    });

    localStorage.setItem('vwMetricsSelector-step', step);

    if (step) {
      const query = { step };
      httpClient
        .get('../api/validation-workflow/v1/system/step', {query})
        .then(resp => {
          const metricsOptions = resp.locations.map(location => {
            return {
              label: location.title,
              options: location.metrics.map(metric => {
                return {
                  _location: location.id,
                  _metric: metric,
                  label: metric,
                };
              }),
            };
          });
          dispatch({
            type: types.INIT_METRICS,
            metrics: metricsOptions,
          });
        })
        .catch(err => {
          dispatch(showApiError(err, '/system/step'));
        });
    }
  };
};

const onChangeLocation = location => {
  return (dispatch, getState) => {
    const state = getState();

    localStorage.setItem('vwMetricsSelector-location', location);

    const locationData = state.globals.locations.find(l => l.id === location);
    if(locationData) {
      dispatch({
        type: types.SET_LOCATION,
        location: locationData.id,
        locationFrequency: locationData.frequency,
      });
      dispatch(loadMetricState(location, state.selector.timeRange));
    }
  };
};

const onChangeShowOriginal = showOriginal => {
  return {
    type: types.SET_SHOW_ORIGINAL,
    showOriginal,
  };
};

const onChangeShowQcode = showQcode => {
  return {
    type: types.SET_SHOW_QCODE,
    showQcode,
  };
};

const onChangeMetrics = (selectedMetrics, primary) => {
  return (dispatch, getState) => {
    const state = getState();

    const step = state.selector.step;
    const metrics = state.selector.metrics;
    const newMetrics = metrics
      .filter(m => m.primary !== primary)
      .concat(
        selectedMetrics.map(m => {
          return {
            step: step,
            location: m._location,
            metric: m._metric,
            primary: primary,
          };
        })
      );
    dispatch({
      type: types.SET_METRICS,
      metrics: newMetrics,
    });
  };
};

const onChangeMetricsPrimary = selectedMetricsPrimary => {
  return onChangeMetrics(selectedMetricsPrimary, true);
};

const onChangeMetricsSecondary = selectedMetricsSecondary => {
  return onChangeMetrics(selectedMetricsSecondary, false);
};

const setTransform = (brushRange, transform) => {
  const transformationParams = {};
  transform.params.forEach(param => {
    transformationParams[param.name] = param.default;
  });

  return {
    type: types.SET_EDITOR_TRANSFORMS,
    transforms: [
      {
        start: brushRange.start,
        end: brushRange.end,
        transformation: transform.id,
        transformationParams: transformationParams,
      },
    ],
  };
};

const onChangeTransformParam = (transformIndex, param, value) => {
  return (dispatch, getState) => {
    const state = getState();
    const newTransforms = cloneDeep(state.editor.transforms);
    if (value.length === 0) value = undefined;
    newTransforms[transformIndex].transformationParams[param] = value;
    dispatch({
      type: types.SET_EDITOR_TRANSFORMS,
      transforms: newTransforms,
    });
  };
};

const saveTransforms = (metrics, editorTransforms) => {
  return (dispatch, getState) => {
    const state = getState();
    const httpClient = state.services.httpClient;
    const toastNotifications = state.services.toastNotifications;

    dispatch({
      type: types.START_EDITOR_TRANSFORMS_SAVING,
    });

    const data = [];
    const primaryMetrics = metrics.filter(metric => metric.primary);
    const totalBrushLength = editorTransforms.reduce(
      (total, editorTransform) => total + getBrushLength(editorTransform),
      moment.duration(0)
    );
    const mode =
      totalBrushLength * primaryMetrics.length < interactiveModeTreashold ? 'interactive' : 'batch';

    primaryMetrics.forEach(metric => {
      editorTransforms.forEach(editorTransform => {
        data.push({
          location: metric.location,
          step: metric.step,
          metric: metric.metric,
          start: DateMath.parse(editorTransform.start, {roundUp: false}).toISOString(),
          end: DateMath.parse(editorTransform.end, {roundUp: true}).toISOString(),
          transformation: editorTransform.transformation,
          transformation_params: editorTransform.transformationParams,
          mode: mode,
        });
      });
    });

    httpClient
      .post('../api/validation-workflow/v1/workflow/transformation', {body: JSON.stringify(data), headers: {"Content-Type": "application/json"}})
      .then(() => {
        dispatch({
          type: types.END_EDITOR_TRANSFORMS_SAVING,
          success: true,
        });
        if (mode === 'interactive') {
          //force refresh of visualization
          //TODO
        } else if (mode === 'batch') {
          toastNotifications.add({
            title: 'Processing launched in batch mode',
            text: 'Use refresh button to see current state',
          });
        }
      })
      .catch(err => {
        dispatch({
          type: types.END_EDITOR_TRANSFORMS_SAVING,
          success: false,
        });
        dispatch(showApiError(err, '/workflow/modify'));
      });
  };
};

const saveValidated = (metrics, editorTransforms) => {
  return (dispatch, getState) => {
    const state = getState();
    const httpClient = state.services.httpClient;

    dispatch({
      type: types.START_EDITOR_VALIDATION_SAVING,
    });

    const data = [];
    const primaryMetrics = metrics.filter(metric => metric.primary);
    primaryMetrics.forEach(metric => {
      editorTransforms.forEach(editorTransform => {
        data.push({
          location: metric.location,
          step: metric.step,
          metric: metric.metric,
          start: DateMath.parse(editorTransform.start, {roundUp: false}).toISOString(),
          end: DateMath.parse(editorTransform.end, {roundUp: true}).toISOString(),
          revoke: false,
        });
      });
    });

    httpClient
      .post('../api/validation-workflow/v1/workflow/validation', {body: JSON.stringify(data), headers: {"Content-Type": "application/json"}})
      .then(() => {
        dispatch({
          type: types.END_EDITOR_VALIDATION_SAVING,
        });
      })
      .catch(err => {
        dispatch({
          type: types.END_EDITOR_VALIDATION_SAVING,
        });
        dispatch(showApiError(err, '/workflow/validate'));
      });
  };
};

const initGlobals = () => {
  return (dispatch, getState) => {
    const state = getState();
    const httpClient = state.services.httpClient;

    httpClient
      .get('../api/validation-workflow/v1/system')
      .then(resp => {
        dispatch({
          type: types.INIT_GLOBALS,
          airflowBaseUrl: resp.airflow_base_url,
          locations: resp.locations,
          transforms: resp.transforms,
        });

        const location = localStorage.getItem('vwMetricsSelector-location');
        if (location) {
          dispatch(onChangeLocation(location));
        }

        const step = localStorage.getItem('vwMetricsSelector-step');
        if (step) {
          dispatch(onChangeStep(step));
        }
      })
      .catch(err => {
        dispatch(showApiError(err, '/system'));
      });
  };
};

const openDataInspector = () => {
  return (dispatch, getState) => {
    const state = getState();
    if(state.vwVisHandler) {
      state.vwVisHandler.openDataInspector();
    }
  }
};

const initVwVisHandler = (vwVisHandler) => {
  return {
    type: types.INIT_VW_VIS_HANDLER,
    vwVisHandler
  }
};

export default {
  showApiError,
  initGlobals,
  onTimeChange,
  onBrushChange,
  onChangeStep,
  onChangeLocation,
  onChangeShowOriginal,
  onChangeShowQcode,
  onChangeMetricsPrimary,
  onChangeMetricsSecondary,
  loadMetricState,
  loadTransforms,
  setTransform,
  onChangeTransformParam,
  saveTransforms,
  saveValidated,
  openDataInspector,
  initVwVisHandler,
};
