import moment from 'moment';

export const getBrushLength = brushRange => {
  if (brushRange.start && brushRange.end) {
    return moment.duration(moment(brushRange.end) - moment(brushRange.start));
  } else {
    return null;
  }
};
