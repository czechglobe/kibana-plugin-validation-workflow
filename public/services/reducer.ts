import {types} from './types';
import {cloneDeep} from 'lodash';

export default (
  state = {
    services: {
      httpClient: undefined,
      toastNotifications: undefined,
      embeddable: undefined,
      savedObjects: undefined,
    },
    globals: {
      airflowBaseUrl: undefined,
      transforms: [],
      locations: [],
    },
    selector: {
      timeRange: {
        start: 'now-1y',
        end: 'now',
      },
      brushRange: {
        start: undefined,
        end: undefined,
      },
      step: undefined,
      location: undefined,
      locationFrequency: '10m',
      metrics: [],
      showOriginal: false,
      showQcode: false,
    },
    metrics: [],
    metricStatus: undefined,
    editor: {
      transforms: [],
      transformsChanged: false,
      transformsSaving: false,
      validateSaving: false,
    },
    vwVisHandler: undefined,
  },
  action
) => {
  const newState = cloneDeep(state);

  switch (action.type) {
    case types.SETUP:
      newState.services.httpClient = action.httpClient;
      newState.services.toastNotifications = action.toastNotifications;
      newState.services.embeddable = action.embeddable;
      newState.services.savedObjects = action.savedObjects;
      break;

    case types.INIT_GLOBALS:
      newState.globals.airflowBaseUrl = action.airflowBaseUrl;
      newState.globals.locations = action.locations;
      newState.globals.transforms = action.transforms;
      break;

    case types.INIT_METRICS:
      newState.metrics = action.metrics;
      break;

    case types.INIT_METRIC_STATUS:
      newState.metricStatus = action.metricStatus;
      break;

    case types.INIT_EDITOR_TRANSFORMS:
      newState.editor.transforms = action.transforms;
      newState.editor.transformsChanged = false;
      break;

    case types.INIT_VW_VIS_HANDLER:
      newState.vwVisHandler = action.vwVisHandler;
      break;

    case types.SET_TIME_RANGE:
      newState.selector.timeRange.start = action.start;
      newState.selector.timeRange.end = action.end;
      break;

    case types.SET_BRUSH_RANGE:
      newState.selector.brushRange.start = action.start;
      newState.selector.brushRange.end = action.end;
      break;

    case types.SET_STEP:
      newState.selector.step = action.step;
      break;

    case types.SET_LOCATION:
      newState.selector.location = action.location;
      newState.selector.locationFrequency = action.locationFrequency;
      break;

    case types.SET_SHOW_ORIGINAL:
      newState.selector.showOriginal = action.showOriginal;
      break;

    case types.SET_SHOW_QCODE:
      newState.selector.showQcode = action.showQcode;
      break;

    case types.SET_METRICS:
      newState.selector.metrics = action.metrics;
      break;

    case types.SET_EDITOR_TRANSFORMS:
      newState.editor.transforms = action.transforms;
      newState.editor.transformsChanged = true;
      break;

    case types.START_EDITOR_TRANSFORMS_SAVING:
      newState.editor.transformsSaving = true;
      break;

    case types.END_EDITOR_TRANSFORMS_SAVING:
      newState.editor.transformsSaving = false;
      newState.editor.transformsChanged = !action.success;
      break;

    case types.START_EDITOR_VALIDATION_SAVING:
      newState.editor.validationSaving = true;
      break;

    case types.END_EDITOR_VALIDATION_SAVING:
      newState.editor.validationSaving = false;
      break;

    default:
      return state;
  }

  return newState;
};
