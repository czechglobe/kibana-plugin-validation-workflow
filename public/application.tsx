import React from 'react';
import ReactDOM from 'react-dom';
import {I18nProvider} from '@kbn/i18n/react';
import {Provider} from 'react-redux';
import {AppMountParameters, CoreStart} from '../../../src/core/public';
import {AppPluginStartDependencies} from './types';
import VwApp from './components/vwApp';
import reducer from './services/reducer';
import {types} from './services/types';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import creators from './services/creators';

export const renderApp = (
  { notifications, http, savedObjects }: CoreStart,
  { navigation, data, embeddable, visualizations }: AppPluginStartDependencies,
  { element }: AppMountParameters,

) => {
  const store = createStore(reducer, applyMiddleware(thunk));

  store.dispatch({
    type: types.SETUP,
    httpClient: http,
    toastNotifications: notifications.toasts,
    embeddable: embeddable,
    savedObjects: savedObjects.client,
  });
  setTimeout(() => {
    store.dispatch(creators.initGlobals());
  });

  ReactDOM.render(
    <Provider store={store}>
      <I18nProvider>
        <VwApp />
      </I18nProvider>
    </Provider>,
    element
  );

  return () => ReactDOM.unmountComponentAtNode(element);
};
