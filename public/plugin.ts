import {i18n} from '@kbn/i18n';
import {AppMountParameters, CoreSetup, CoreStart, Plugin,} from '../../../src/core/public';
import {AppPluginStartDependencies, ValidationWorkflowPluginSetup, ValidationWorkflowPluginStart,} from './types';
import {PLUGIN_ID, PLUGIN_NAME} from '../common';

import logoCzechGlobe from './logoCzechGlobe.svg'

export class ValidationWorkflowPlugin
  implements Plugin<ValidationWorkflowPluginSetup, ValidationWorkflowPluginStart> {
  public setup(core: CoreSetup): ValidationWorkflowPluginSetup {
    core.application.register({
      id: PLUGIN_ID,
      title: PLUGIN_NAME,
      order: 9000,
      euiIconType: 'lensApp',
      category: {
        id: 'czechGlobe',
        label: "CzechGlobe",
        euiIconType: logoCzechGlobe,
        order: 500,
      },
      async mount(params: AppMountParameters) {
        const { renderApp } = await import('./application');
        const [coreStart, depsStart] = await core.getStartServices();
        return renderApp(coreStart, depsStart as AppPluginStartDependencies, params);
      },
    });

    // Return methods that should be available to other plugins
    return {};
  }

  public start(core: CoreStart): ValidationWorkflowPluginStart {
    return {};
  }

  public stop() {}
}
