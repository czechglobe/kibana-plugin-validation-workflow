import {NavigationPublicPluginStart} from '../../../src/plugins/navigation/public';
import {DataPublicPluginStart} from '../../../src/plugins/data/public';
import {EmbeddableStart} from '../../../src/plugins/embeddable/public';
import {VisualizationsStart} from '../../../src/plugins/visualizations/public';

export interface ValidationWorkflowPluginSetup {

}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ValidationWorkflowPluginStart {}

export interface AppPluginStartDependencies {
  navigation: NavigationPublicPluginStart;
  data: DataPublicPluginStart;
  embeddable: EmbeddableStart,
  visualizations: VisualizationsStart,
}
